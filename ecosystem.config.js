module.exports = {
  apps: [
    {
      name: 'dna-api',
      script: 'npm',
      args: 'start',
    },
  ],
};
