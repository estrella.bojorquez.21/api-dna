const express = require("express");
const { mongoose } = require('mongoose');
const cors = require("cors")
const dotenv = require("dotenv");
const dnaRoutes = require("./routes/dna.route")


dotenv.config();
const app = express();
const port = process.env.PORT || 5000;


mongoose
  .connect(process.env.MONGO_URL)
  .then(() => console.log('DB connection sucessfull :D'))
  .catch((error) => console.log('DB connection failed \n\n' + error));


app.use(cors());
app.use(express.json());
app.use('/', dnaRoutes);

app.listen(
  port, 
() => console.log(`API escuchando en puerto: ${port} `))

