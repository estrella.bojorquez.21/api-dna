
function square(arr) {
  const arrLength = arr.length;
  let columns = 0;
  for (let index = 0; index < arrLength; index++) {
    if(arr[index].length ==arrLength ) {
      columns++;
    }  
  }
  if(columns == arrLength) {
    return true;
  }
  else { return false}
}


const validLetters = function(arr) {
  const regex = /[^ATGC]/g;
  let validElement = 0;
 arr.forEach(element => {
  if( element.match(regex) == null) validElement++;
 });
 return validElement == arr.length;
}

function squareMatrix(req, res, next) {
  const arr = req.body.dna;
  if(square(arr)) {
    return next();
  }
 return res.status(422).send({ message: "La matriz no es cuadrada"});
}
function validElements(req, res, next) {
  const arr = req.body.dna;
  if(validLetters(arr)) {
    return next();
  }
  return res.status(422).send({ message: "Solo se admiten caracteres A, C, T, G"});
}
module.exports = {
  square,
  validLetters,
  squareMatrix,
  validElements

}