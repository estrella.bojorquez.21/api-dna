const mutation = require("../services/mutation.service")
const Dna = require('../models/Dna');

async function dnaMutation(req, res) {
  try {
    const hasMutation = mutation.hasMutation(req.body.dna);
    let status = null;
    let message = '';

    if (hasMutation == true) {
      status = 200;
      message = 'Es una mutacion';
    } else {
      status = 403;
      message = 'No es una mutacion';
    }
    const dnaChain = req.body.dna.join('|');
    // console.log(dnaChain);

    const newDna = new Dna();
    newDna.chain = dnaChain;
    newDna.hasMutation = hasMutation;
    await newDna.save((err) => {
      if (err && err.code == 11000) {
        console.log(err.code, 'Cadena no guardada');
      }
    });
    return res.status(status).send({ message });
  } catch (error) {
    return res.status(500).send({ error });
  }
}

async function getStats(req, res) {
  try {
    const countNoMutations = await Dna.countDocuments({ hasMutation: false });
    const countHasMutations = await Dna.countDocuments({ hasMutation: true });

    const ratio = parseFloat((countHasMutations / countNoMutations).toFixed(2));

    console.log(countHasMutations, countNoMutations);
    const stats = {
      count_mutations: countHasMutations,
      count_no_mutation: countNoMutations,
      ratio,
    };
    return res.status(200).send(stats);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ error });
  }
}
module.exports = {
  dnaMutation,
  getStats,
};