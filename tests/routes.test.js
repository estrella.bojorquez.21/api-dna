const request = require('supertest');
const express = require("express");
const cors = require("cors")
// const mongoose = require('mongoose');

const dbHandler = require('./db-handler.js');

const dnaRoutes = require('../routes/dna.route');

beforeAll(async () => await dbHandler.dbConnect());
afterEach(async () => await dbHandler.clearDatabase());
afterAll(async () => await dbHandler.closeDatabase());


const app = express();
app.use(cors());
app.use(express.json());
app.use('/', dnaRoutes);

describe('GET Endpoints', () => {
  it('prueba', async () => {
    const res = await request(app)
      .get('/mutations')
      
    expect(res.statusCode).toBe(201)
  })
})

describe('POST endpoints', () => {
  it('Comprueba que hay mutacion', async() => {
    const res = await request(app)
    .post('/mutation')
    .send( {
      dna : 
      [
        "ATGCGA",
        "CAGTAC",
        "TTATGT",
        "AGAAGG",
        "CCCCTA",
        "TCACTG"
      ]
      }
 );
     expect(res.statusCode).toBe(200)
  });
  it('Comprueba que NO hay una mutacion', async() => {
    const res = await request(app)
    .post('/mutation')
    .send( {
      dna : 
      [
        "ATGCGA",
        "CAGTGC",
        "TTATTT",
        "AGACGG",
        "GCGTCA",
        "TCACTG"
      ]
      }
 );
     expect(res.statusCode).toBe(403)
  });
  it('Error de validacion', async() => {
    const res = await request(app)
    .post('/mutation')
    .send( {
      dna : 
      [
        "ATGCGA",
        "CGTGC",
        "TTATTT",
        "AGACGG",
        "GCGTCA",
        "TCACTG"
      ]
      }
 );
     expect(res.statusCode).toBe(422)
  });
  
})