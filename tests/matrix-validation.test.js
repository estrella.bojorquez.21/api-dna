const matrixValidation = require("../middlewares/matrix-validation");


describe('Test matrix-validation', () => {
  test("Funciona con una matriz cuadrada", () => {
    const DNA = [
      "ATGCGA",
      "CAGTAC",
      "TTATGT",
      "AGAAGG",
      "CCCCTA",
      "TCACTG"
    ];
  
    expect(matrixValidation.square(DNA)).toBe(true);
  });
  test("No funciona con una matriz no-cuadrada", () => {
    const DNA = [
      "ATGCGA",
      "CAGTAC",
      "TTATGT",
      "AGAAGG",
      "CCCCTA",
      "TCACTG",
      "TCACTG"
    ];
    expect(matrixValidation.square(DNA)).toBe(false);
  })
  test("Funciona solo con las letras A, C, G, T", () => {
    const DNA = [
      "ATGCGA",
      "CAGTAC",
      "TTATGT",
      "AGAAGG",
      "CCCCTA",
      "TCACTG",
      "TCACTG"
    ];
    expect(matrixValidation.validLetters(DNA)).toBe(true);
  })
  test("No funciona solo con letras diferentes a: A, C, G, T", () => {
    const DNA = [
      "ATGCGA",
      "CAGTPC",
      "TTATGT",
      "AGAAGG",
      "CCCCTA",
      "TCACTG",
      "TCACTG"
    ];
    expect(matrixValidation.validLetters(DNA)).toBe(false);
  })
})
