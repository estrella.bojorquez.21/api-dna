const mutationService = require("../services/mutation.service");
const DNA = [
  "ATGCGA",
  "CAGTAC",
  "TTATGT",
  "AGAAGG",
  "CCCCTA",
  "TCACTG"
];
const DNA2 = [
  "ATGCGA",
  "CAGTGC",
  "TTATTT",
  "AGACGG",
  "GCGTCA",
  "TCACTG"
];

describe('Test mutation.service', () => {
  test("Devuelve un valor", () => {
    expect(mutationService.sequenceFound(DNA[0])).toBeTruthy();
  });
  test("Detecta que hay una mutacion", () => {
    expect(mutationService.hasMutation(DNA)).toBe(true);
  });
  test("Detecta que NO hay una mutacion", () => {
    expect(mutationService.hasMutation(DNA2)).toBe(false);
  });
})