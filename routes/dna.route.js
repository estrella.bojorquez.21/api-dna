const express = require("express");
const dnaController = require("../controllers/dna.controller")
const md_matrix = require("../middlewares/matrix-validation")
const router = express.Router();

router.get('/', (req, res) => {
  return res.status(200).send('API de validación de mutaciones de DNA');
});
router.get('/mutations', (req, res) => {
  res.status(201).send('test');
});
router.get('/stats', dnaController.getStats);
router.post('/mutation', md_matrix.squareMatrix, md_matrix.validElements, dnaController.dnaMutation)

module.exports = router;