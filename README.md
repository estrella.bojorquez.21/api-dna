# API-DNA

Este proyecto sirve para detectar si una persona tiene diferencias genéticas basándose en su secuencia de ADN. Brinda estadísticas de las secuencias verificadas.

URL raíz demo: http://137.184.178.130:5000/

## Requerimientos

Instalar [MongoDB](https://www.mongodb.com/try/download/community)

Instalar [Node.js](https://nodejs.org/es/download/)

## Instalación
Instalar dependencias
```bash
npm install
```
Copiar el archivo .env.example
```bash
cp .env.example .env
```
Editar variables PORT (puerto en el que se ejecutará la aplicación) y MONGO_URL (cadena de conexión de base de datos de mongo)


## Uso
### Desarrollo
Para ejecutar la aplicación utilizando nodemon

```bash
npm run dev

```
Para correr los tests 
```bash
npm run test-dev
```

### Producción
Para ejecutar la aplicación
```bash
npm run start
```
Para correr los tests
```
npm run test
```


## Ejemplos
`POST http://localhost:PORT/mutation
{
 "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}`
```
➜ api-dna git:(master)✗ curl -X POST -H "Content-Type: application/json" -d '{\"dna\":[\"ATGCGA\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"]}' http://137.184.178.130:5000/mutation

{"message":"Es una mutacion"}
```

`GET http://localhost:PORT/stats`
```
➜ api-dna git:(master)✗ curl -X GET -H "Content-Type: application/json" http://137.184.178.130:5000/stats

{"stats":{"count_mutations":1,"count_no_mutation":3,"ratio":0.33}}
```
## License
[MIT](https://choosealicense.com/licenses/mit/)

made with ❤ by EstrellaPB
