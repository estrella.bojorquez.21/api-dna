const { Schema, model } = require('mongoose');

const dnaSchema = Schema({
  chain: {
    type: String,
    required: true,
    unique: true,
  },
  hasMutation: {
    type: Boolean,
    required: true,
    default: false,
  },
});

module.exports = model('Dna', dnaSchema);
