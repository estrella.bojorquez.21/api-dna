const sequenceFound = function (string) {
  return string.split("").reduce((acumulator, currentLetter, idx, arr) => {
    const lastLetter = arr[idx - 1];
    const isEqualToLast = currentLetter == lastLetter;

    if (acumulator < 4 && isEqualToLast) {
      return acumulator + 1;
    } else if (acumulator < 4) {
      return 1;
    }
    return acumulator;
  }, 1);
};


const hasMutation = function (dna) {
  let mutationsFound = 0;
  const n = dna.length;
  // * Diagonales 1
  for (let column = 0; column < n - 3; column++) {
    const diagonalLT = dna.map((e, i) => {
      return e[i + column];
    }).join('');
    if(sequenceFound(diagonalLT) == 4 ) {
      mutationsFound++;
    }
    // console.log(`diagonalLT[${column}]: `, diagonalLT);
    const diagonalR = dna.map((e, i) => {
      return e[n-1-i+column];
    }).join('');
    if( sequenceFound(diagonalR) == 4 ){
      mutationsFound++;
    }
    // console.log(`DiagonalRD[${column}]:`, diagonalR);
  }
// * Diagonales 2
  for (let row = 1; row < n - 3; row++) {
    const rowDNA = dna.slice(row, n);
    const diagonalLD = rowDNA.map((e, i) => {
      return e[i + row - 1];
    }).join('');
    if(sequenceFound(diagonalLD) == 4) {
      mutationsFound++;
    }
    // console.log('diagonalLD: ', diagonalLD);
    const diagonalRT = dna.map((e,i) => {
      return e[n-row-i-1]
    }).join('');
    if( sequenceFound(diagonalRT) == 4) {
      mutationsFound++;
    }
    // console.log('DiagonalRT:', diagonalRT)
  }

// * Verticales
// ? Verticales y horizontales
  for (let column = 0; column < n; column++) {
    const vertical = dna.map((e, i) => {
      return e[n-1-column];
    }).join('');
    if(sequenceFound(vertical) == 4  || sequenceFound(dna[column]) == 4 ){
      mutationsFound++;
    }
    // console.log(`Verticales[${column}]:`, vertical);
    // const horizontal = console.log(`Horizontal[${column}]: ${dna[column]}`)

  }

  return mutationsFound >= 2;
};

module.exports = {
  sequenceFound, 
  hasMutation
}